@extends('adminlte.master');

@section('head-title')
    <h1>Casts</h1>
@endsection

@section('title')
    <h3 class="card-title">Data Casts</h3>
@endsection

@section('content')
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Data Casts</h3>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    @if (session('success'))
    <div class="alert alert-success">
      {{ session('success') }}
    </div>
    @endif
    @if (session('success-delete'))
    <div class="alert alert-danger">
      {{ session('success-delete') }}
    </div>
    @endif
    <table id="example1" class="table table-bordered table-striped">
      <thead>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Bio</th>
        <th>Actions</th>
      </tr>
      </thead>
      <tbody>
        
      @forelse ($casts as $key=>$cast)
      <tr>
        <td>{{ $key + 1 }}</td>
        <td>{{ $cast->nama }}</td>
        <td>{{ $cast->umur }}</td>
        <td>{{ $cast->bio }}</td>
        <td>
          <a href="/cast/{{ $cast->id }}" class="btn btn-info">Show</a> &nbsp;
          <a href="/cast/{{ $cast->id }}/edit" class="btn btn-primary">Edit</a> &nbsp;
          <form action="/cast/{{ $cast->id }}" method="POST">
            @csrf
            @method('DELETE')
            <input onclick="return confirm('are you sure to delete?')" type="submit" class="btn btn-danger my-1" value="Delete">
          </form>
        </td>
      </tr>
      @empty
        <tr colspan="3">
          <td>No data</td>
        </tr>  
      @endforelse 

      </tbody>
      <tfoot>
      <tr>
        <th>No</th>
        <th>Nama</th>
        <th>Umur</th>
        <th>Bio</th>
        <th>Actions</th>
      </tr>
      </tfoot>
    </table>
  </div>
  <!-- /.card-body -->
</div>
@endsection

@push('scripts')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush