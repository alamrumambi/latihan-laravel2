@extends('adminlte.master');

@section('head-title')
    <h1>Detail Cast</h1>
@endsection

@section('title')
    <h3 class="card-title">{{ $cast->nama }}</h3>
@endsection

@section('content')
<form>
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Nama</label>
    <input type="text" disabled value="{{ $cast->nama }}" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Umur</label>
    <input disabled type="text" value="{{ $cast->umur }}" name="umur" class="form-control" id="exampleInputPassword1">
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword2" class="form-label">Bio</label>
    <input disabled type="text" value="{{ $cast->bio }}" name="bio" class="form-control" id="exampleInputPassword2">
  </div>
</form>
<a href="/cast" class="btn btn-primary">Back</a>
@endsection