@extends('adminlte.master');

@section('head-title')
    <h1>Add New Cast</h1>
@endsection

@section('title')
    <h3 class="card-title">Add Cast</h3>
@endsection

@section('content')
<form action="/cast" method="POST">
  @csrf
  @error('nama')
  <div class="alert alert-danger">
      {{ $message }}
  </div>
  @enderror
  <div class="mb-3">
    <label for="exampleInputEmail1" class="form-label">Nama</label>
    <input type="text" name="nama" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
  </div>
  @error('umur')
  <div class="alert alert-danger">
      {{ $message }}
  </div>
  @enderror
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Umur</label>
    <input type="text" name="umur" class="form-control" id="exampleInputPassword1">
  </div>
  @error('bio')
  <div class="alert alert-danger">
      {{ $message }}
  </div>
  @enderror
  <div class="mb-3">
    <label for="exampleInputPassword2" class="form-label">Bio</label>
    <input type="text" name="bio" class="form-control" id="exampleInputPassword2">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection