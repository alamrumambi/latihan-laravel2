<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class tableController extends Controller
{
    //
    public function showTable() {
        return view('table');
    }

    public function showDataTable() {
        return view('data-table');
    }
}
