<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class castController extends Controller
{
    //
    public function index() {
        $casts = DB::table('casts')->get();
        return view('table-casts', compact('casts'));
    }

    public function create() {
        return view('add-form');
    }

    public function store(Request $req) {
        $req->validate([
            'nama' => 'required|unique:casts',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('casts')->insert([
            "nama" => $req["nama"],
            "umur" => $req["umur"],
            "bio" => $req["bio"]
        ]);
        return redirect('/cast')->with('success', 'New cast added successfully');
    }

    public function show($cast_id, Request $req) {
        $cast = DB::table('casts')->where('id', $cast_id)->first();
        return view('detail-form', compact('cast'));
    }

    public function edit($cast_id, Request $req) {
        $cast = DB::table('casts')->where('id', $cast_id)->first();
        return view('edit-form', compact('cast'));
    }

    public function update($cast_id,Request $req) {
        $req->validate([
            'nama' => 'required',
            'umur' => 'required',
            'bio' => 'required'
        ]);
        $query = DB::table('casts')
            ->where('id', $cast_id)
            ->update([
            "nama" => $req["nama"],
            "umur" => $req["umur"],
            "bio" => $req["bio"]
        ]);
        return redirect('/cast')->with('success', "cast with id $cast_id updated successfully");
    }

    public function destroy($cast_id)
    {
        $query = DB::table('casts')->where('id', $cast_id)->delete();
        return redirect('/cast')->with('success-delete', "cast with id $cast_id deleted successfully");
    }
}
